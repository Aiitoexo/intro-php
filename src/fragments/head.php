<?php

if (empty($title)) {
    exit('Erreur il faut ecrire une variable $title pour afficher la page');
}

?>

<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<base href="http://localhost/intro-PHP/">
<link rel="stylesheet" href="css/app.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Rubik&display=swap" rel="stylesheet">
<title><?= $title ?></title>
