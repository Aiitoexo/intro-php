<div class="container-body portfolio">
    <div class="portfolio-row-1">
        <div>
            <h3>PORTFOLIO</h3>
            <p>Far far away,behind the world mountains, far from the countries Vokalia.</p>
        </div>
        <div class="zoom-img">
            <p>WORK 01</p>
            <img src="img/image-portefolio/image_3.jpg" alt="">
            <img src="img/loupe.png" alt="">
        </div>
        <div class="zoom-img">
            <p>WORK 02</p>
            <img src="img/image-portefolio/image_2.jpg" alt="">
            <img src="img/loupe.png" alt="">
        </div>
        <div class="zoom-img">
            <p>WORK 03</p>
            <img src="img/image-portefolio/image_9.jpg" alt="">
            <img src="img/loupe.png" alt="">
        </div>
    </div>
    <div class="portfolio-row-2">
        <div class="zoom-img">
            <p>WORK 04</p>
            <img src="img/image-portefolio/image_1.jpg" alt="">
            <img src="img/loupe.png" alt="">
        </div>
        <div>
            <div class="zoom-img">
                <p>WORK 05</p>
                <img src="img/image-portefolio/image_5.jpg" alt="">
                <img src="img/loupe.png" alt="">
            </div>
            <div class="zoom-img">
                <p>WORK 06</p>
                <img src="img/image-portefolio/image_6.jpg" alt="">
                <img src="img/loupe.png" alt="">
            </div>
        </div>
        <div>
            <div class="zoom-img">
                <p>WORK 07</p>
                <img src="img/image-portefolio/image_7.jpg" alt="">
                <img src="img/loupe.png" alt="">
            </div>
            <div class="zoom-img">
                <p>WORK 08</p>
                <img src="img/image-portefolio/image_4.jpg" alt="">
                <img src="img/loupe.png" alt="">
            </div>
        </div>
    </div>
</div>


