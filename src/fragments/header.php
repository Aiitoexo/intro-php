<div class="nav hide">
    <div>
        <p>[TOUGH]</p>
        <ul>
            <li><span>01</span><a href="home">Home</a></li>
            <li><span>02</span><a href="about">About</a></li>
            <li><span>03</span><a href="services">Services</a></li>
            <li><span>04</span><a href="portfolio">Portfolio</a></li>
            <li><span>05</span><a href="contact">Contact</a></li>
        </ul>
    </div>

    <div class="cross"></div>
</div>

<div class="test">
    <div class="hamburger-menu"></div>
</div>

<div class="header">
    <div class="title-header">
        <h1>[<span>TOUGH</span>]</h1>
    </div>
    <div class="img-header">
        <img src="img/image_10-1.jpg" alt="">
        <p>#<span>01 EXTERIEUR BUILDING</span></p>
    </div>
    <div class="img-header">
        <img src="img/image_10-2.jpg" alt="">
        <p>#<span>02 LOUIE'S RESIDENCES</span></p>
    </div>
    <div class="img-header">
        <img src="img/image_10-3.jpg" alt="">
        <p>#<span>03 BUSINESS BUILDING</span></p>
    </div>
</div>
