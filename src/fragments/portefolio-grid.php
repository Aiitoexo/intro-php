<div class="container-body portfolio-grid">

    <?php

    $table_image = glob('../public/img/image-portefolio/image_*.jpg');

    for ($i = 1; $i <= 100; $i++): $random_image = array_rand($table_image); ?>
        <?php
        $file_image = '../public/img/image-portefolio/image_' . $random_image . '.jpg';
        if (file_exists($file_image)): ?>

            <div class="zoom-img-grid">
                <p>WORK <?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></p>
                <img src="img/image-portefolio/image_<?= $random_image ?>.jpg" alt="">
                <img src="img/loupe.png" alt="">
            </div>

        <?php else: ?>

            <div class="zoom-img-grid">
                <p>WORK <?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></p>
                <img src="img/image-portefolio/image_<?= $random_image + 1 ?>.jpg" alt="">
                <img src="img/loupe.png" alt="">
            </div>

        <?php endif ?>
    <?php endfor ?>

</div>

