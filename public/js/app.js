const servicesLI = document.querySelectorAll('.menu-services li')
const sevicesContent = document.querySelectorAll('.content-services div')
const triangleLI = document.querySelectorAll('.triangle')
const hamburgerButton = document.querySelector('.test')
const navbar = document.querySelector('.nav')
const cross = document.querySelector('.cross')

for (let i = 0; i < servicesLI.length; i++) {
    servicesLI[i].addEventListener('mouseover',function (){
        for (let i = 0; i < sevicesContent.length; i++) {
            triangleLI[i].classList.remove('show')
            triangleLI[i].classList.add('hide')
            sevicesContent[i].classList.remove('show')
            sevicesContent[i].classList.add('hide')
        }
        triangleLI[i].classList.remove('hide')
        triangleLI[i].classList.add('show')
        sevicesContent[i].classList.remove('hide')
        sevicesContent[i].classList.add('show')
    })
}

hamburgerButton.addEventListener('click', function () {
    navbar.classList.remove('hide')
    navbar.classList.add('show')
})

navbar.addEventListener('mouseleave', function () {
    navbar.classList.add('hide')
})

cross.addEventListener('click', function () {
    navbar.classList.remove('show')
    navbar.classList.add('hide')
})

