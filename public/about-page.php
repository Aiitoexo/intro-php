<!doctype html>
<html lang="en">
    <head>
        <?php $title = 'about'; require '../src/fragments/head.php' ?>
    </head>
    <body>

        <?php require '../src/fragments/header.php' ?>

        <div class="title-page">
            <h2>ABOUT</h2>
        </div>

        <div class="container-body about">
            <div>
                <img src="public/img/person_3.jpg" alt="">
                <ul>
                    <li>Date Creation : </li>
                    <li>Adress</li>
                </ul>
            </div>
            <div>
                <div>
                    <h3>About Building</h3>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam commodi cupiditate distinctio,
                    dolores impedit inventore, iste iusto magni placeat quaerat quidem rem sequi, sit? Hic maiores quae
                    quas rem sed?</p>
            </div>
        </div>


        <?php require '../src/fragments/footer.php' ?>

        <script src="js/app.js"></script>
    </body>
</html>
