<!doctype html>
<html lang="en">
    <head>
        <?php $title = 'home'; require '../src/fragments/head.php' ?>
    </head>
    <body>

        <?php require '../src/fragments/header.php' ?>

        <div class="container-body">
            <div class="counter-content">
                <div class="counter">
                    <div>
                        <h2>800</h2>
                        <p>FINISHED PROJECTS</p>
                    </div>
                    <div>
                        <h2>795</h2>
                        <p>HAPPY CUSTOMERS</p>
                    </div>
                    <div>
                        <h2>1,200</h2>
                        <p>WORKING HOURS</p>
                    </div>
                    <div>
                        <h2>850</h2>
                        <p>CUPS OF COFFEE</p>
                    </div>
                </div>
                <div class="img-counter">
                    <button type="button">Explore Futher</button>
                    <img src="img/image-portefolio/image_1.jpg" alt="">
                </div>
            </div>
        </div>

        <div class="no-container-body">
            <div class="menu-services">
                <h4><span>SER</span>VICES</h4>
                <ul>
                    <li>
                        <img src="img/interior.svg" alt="">
                        <p>INTERIOR</p>
                        <div class="triangle show"></div>
                    </li>
                    <li>
                        <img src="img/ideas.svg" alt="">
                        <p>CONCEPT</p>
                        <div class="triangle hide"></div>
                    </li>
                    <li>
                        <img src="img/modern-house.svg" alt="">
                        <p>RESIDENTIAL</p>
                        <div class="triangle hide"></div>
                    </li>
                    <li>
                        <img src="img/skyline.svg" alt="">
                        <p>HOSPITALITY</p>
                        <div class="triangle hide"></div>
                    </li>
                </ul>
            </div>
            <div class="content-services">
                <div class="show">
                    <img src="img/interior.svg" alt="">
                    <h3>Interior Design</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda aut beatae cumque,
                        distinctio dolores enim eos ex ipsum minus nam nemo neque obcaecati officia quia voluptas
                        voluptate voluptatem. Exercitationem.
                        enim eos ex ipsum minus nam nemo neque obcaecati officia quia voluptas
                        voluptate voluptatem. Exercitationem.</p>
                    <button type="button">Learn More</button>
                </div>
                <div class="hide">
                    <img src="img/ideas.svg" alt="">
                    <h3>Concept Design</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda aut beatae cumque,
                        distinctio dolores enim eos ex ipsum minus nam nemo neque obcaecati officia quia voluptas

                        enim eos ex ipsum minus nam nemo neque obcaecati officia quia voluptas
                        voluptate voluptatem. Exercitationem.</p>
                    <button type="button">Learn More</button>
                </div>
                <div class="hide">
                    <img src="img/modern-house.svg" alt="">
                    <h3>Residential Design</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda aut beatae cumque,
                        distinctio dolores enim eos ex ipsum minus nam nemo neque obcaecati officia quia voluptas
                        cati officia quia voluptas
                        voluptate voluptatem. Exercitationem.</p>
                    <button type="button">Learn More</button>
                </div>
                <div class="hide">
                    <img src="img/skyline.svg" alt="">
                    <h3>Hospitality Designe</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda aut beatae cumque,
                        s nam nemo neque obcaecati officia quia voluptas
                        voluptate voluptatem. Exercitationem.
                        enim eos ex ipsum minus nam nemo neque obcaecati officia quia voluptas
                        voluptate voluptatem. Exercitationem.</p>
                    <button type="button">Learn More</button>
                </div>
            </div>
        </div>

        <div class="no-container-body">
            <div class="get-started">
                <p>Get Started</p>
                <button>Request a quote</button>
            </div>
        </div>

       <?php require '../src/fragments/portefolio.php' ?>

        <div class="no-container-body comments-client">
            <div class="client-says">
                <div class="container-client">
                    <h3>CLIENTS SAYS</h3>
                    <p>Far far away,behind the world mountains, far from the countries Vokalia</p>
                </div>
            </div>
            <div class="comments">
                <div class="comment">
                    <picture>
                        <img src="img/person_1.jpg" alt="">
                        <div class="quote">
                            <img src="img/quotes.svg" alt="">
                        </div>
                    </picture>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad asperiores harum tenetur totam!
                        Adipisci autem beatae dolore error, eveniet hic incidunt, ipsa iste laborum nihil possimus
                        reprehenderit sunt tenetur voluptate?</p>
                    <p class="name">Dennis Green</p>
                    <p class="job">Architect</p>
                </div>

                <div class="comment">
                    <picture>
                        <img src="img/person_2.jpg" alt="">
                        <div class="quote">
                            <img src="img/quotes.svg" alt="">
                        </div>
                    </picture>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad asperiores harum tenetur totam!
                        Adipisci autem beatae dolore error, eveniet hic incidunt, ipsa iste laborum nihil possimus
                        reprehenderit sunt tenetur voluptate?</p>
                    <p class="name">John Doe</p>
                    <p class="job">Architect</p>
                </div>
            </div>
        </div>

        <div class="container-bottom">
            <div class="container-card">
                <div class="card">
                    <img src="img/skyline.svg" alt="">
                    <h2>Basic Plan</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab animi blanditiis consectetur
                        consequatur !</p>
                    <p>$29<span>per month</span></p>
                    <button type="button">Get started</button>
                    <p>Open Source</p>
                </div>

                <div class="card">
                    <img src="img/skyline.svg" alt="">
                    <h2>Stardard Plan</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab animi blanditiis consectetur
                        consequatur !</p>
                    <p>$29<span>per month</span></p>
                    <button type="button">Get started</button>
                    <p>Free 30 Day Trial</p>
                </div>

                <div class="card">
                    <img src="img/skyline.svg" alt="">
                    <h2>Premium Plan</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab animi blanditiis consectetur
                        consequatur !</p>
                    <p>$29<span>per month</span></p>
                    <button type="button">Get started</button>
                    <p>Request a Quote</p>
                </div>
            </div>
        </div>

        <?php require '../src/fragments/footer.php' ?>

        <script src="js/app.js"></script>
    </body>
</html>
