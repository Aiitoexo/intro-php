<!doctype html>
<html lang="en">
    <head>
        <?php $title = 'contact'; require '../src/fragments/head.php' ?>
    </head>
    <body>

        <?php require '../src/fragments/header.php' ?>

        <div class="title-page">
            <h2>CONTACT</h2>
        </div>

        <?php require '../src/fragments/footer.php' ?>

        <script src="js/app.js"></script>
    </body>
</html>
